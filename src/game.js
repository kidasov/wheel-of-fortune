const slices = [0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27,
    13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31,
    9, 22, 18, 29, 7, 28, 12, 35, 3, 26
];

const SLICE_DEG = 360 / slices.length;

const WHEEL_PARAMETERS = {
	MIN: {
		DURATION_TO_STOP: 3000,
		SPEED : 0.05
	},

	MAX: {
		DURATION_TO_STOP: 5000,
		SPEED : 0.3
	}
};

class HelperFunctions {
    static radToDeg(rad) {
        return rad * 180 / Math.PI;
    }

    static getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
}


class DisplayObject {
    constructor(image) {
        this._group = new PIXI.Container();

        this._sprite = new PIXI.Sprite(PIXI.Texture.fromFrame(image));
        this._sprite.anchor.set(0.5);
        this._group.addChild(this._sprite);
    }

    get group() {
        return this._group;
    }
    get sprite() {
        return this._sprite;
    }
    set sprite(sprite) { this._sprite = sprite; }

    setPosition(x, y) {
        this._group.x = x;
        this._group.y = y;
    }

    setTexture(image) {
        let texture = PIXI.Texture.fromFrame(image);
        this._sprite.setTexture(texture);
    }
}

class Wheel extends DisplayObject {
    constructor(image, speed, duration) {
        super(image);
        this._delta = 0;
        this._duration = duration;
        this._speed = speed;
        this._isFastRotating = false;
        this._isSlowRotating = false;
        this._currentSector = 0;

        this._arrow = new DisplayObject("stopper.png");
        this._arrow.setPosition(0, -this.sprite.height * 0.5);
        this.group.addChild(this._arrow.group);
    }

    get delta() {
        return this._delta;
    }
    set delta(delta) { this._delta = delta; }

    get duration() {
        return this._duration;
    }
    set duration(duration) { this._duration = duration; }

    get speed() {
        return this._speed; }
    set speed(speed) { this._speed = speed; }

    get isFastRotating() {
        return this._isFastRotating;
    }
    set isFastRotating(isFastRotating) { this._isFastRotating = isFastRotating; }

    get isSlowRotating() {
        return this._isSlowRotating;
    }
    set isSlowRotating(isSlowRotating) { this._isSlowRotating = isSlowRotating; }

    get currentSector() {
        return this._currentSector;
    }
    set currentSector(currentSector) { this._currentSector = currentSector; }

    spinInfinite(speed) {
        this.sprite.rotation += speed;
    }

    spin(start, onComplete) {
        let degrees = HelperFunctions.radToDeg(this.sprite.rotation);
        if (this.delta > 1) {
            this.currentSector = slices.length - 1 - Math.floor(((degrees - SLICE_DEG * 0.5) % 360) / SLICE_DEG);
            if (onComplete) {
                onComplete();
            }
            return;
        }
        this.sprite.rotation += this.speed * (1 - this.delta);
    }

    stopSpin(start) {
        let now = (new Date()).getTime();
        this.delta = ((now - start) / this.duration);
    }
}

class Button extends DisplayObject {
    constructor(image) {
        super(image);
        this.sprite.buttonMode = true;
        this.sprite.interactive = true;
        this._text = new PIXI.Text("");
        this.group.addChild(this._text);
        this._text.anchor.set(0.5);

        this.sprite.on("pointerover", () => this.onButtonOver());
        this.sprite.on("pointerout", () => this.onButtonOut());
    }

    onButtonOver() {
        this.isOver = true;
        if (this.isdown) {
            return;
        }

        let texture = PIXI.Texture.fromFrame("button_white_over.png");
        this.sprite.setTexture(texture);
    }

    onButtonOut() {
        this.isOver = false;
        if (this.isdown) {
            return;
        }

        let texture = PIXI.Texture.fromFrame("button_white_disable.png");
        this.sprite.setTexture(texture);
    }

    addOnDown(func) {
        this.sprite.on("pointerdown", func);
    }

    get text() {
        return this._text;
    }
}

class Game {
    constructor(width, height, params) {
        this._app = new PIXI.Application(width, height, { backgroundColor: 0xffffff });
        this._ratio = width / height;
        // this._app.view.style.position = "absolute";
        // this._app.view.style.left = "50%";
        // this._app.view.style.top = "50%";
        // this._app.view.style.transform = "translate3d( -50%, -50%, 0 )";

        this.tickerFunctions = [];
    }

    get stage() {
        return this._app.stage;
    }
    get renderer() {
        return this._app.renderer;
    }
    get ticker() {
        return this._app.ticker;
    }
    set ticker(ticker) { this._ticker = ticker; }
    get width() {
        return this.renderer.width;
    }
    get height() {
        return this.renderer.height;
    }
    get ratio() {
        return this._ratio;
    }


    render(stage) {
        this._app.render(stage);
    }

    get view() {
        return this._app.view;
    }

    addTickerFunction(func) {
        this.tickerFunctions.push(func);
        this.ticker.add(func);
    }

    clearTickerFunctions() {
        this.tickerFunctions.forEach((func, i, tickerFunctions) => {
            this.ticker.remove(func);
        });
    }

    setState(stage) {
        this._app.stage.addChild(stage);
        this._app.render(stage);
    }

    resize() {
        var w, h;
        if (window.innerWidth / window.innerHeight >= this._ratio) {
            w = window.innerHeight * this._ratio;
            h = window.innerHeight;
        } else {
            w = window.innerWidth;
            h = window.innerWidth / this._ratio;
        }
        this._app.view.style.width = w + "px";
        this._app.view.style.height = h + "px";
    }
}

class Scene extends PIXI.Container {
    constructor(game) {
        super();

        this.game = game;
        this.create();
    }

    create() {

        let speed = HelperFunctions.getRandomArbitrary(WHEEL_PARAMETERS.MIN.SPEED, WHEEL_PARAMETERS.MAX.SPEED);
        let duration = HelperFunctions.getRandomArbitrary(WHEEL_PARAMETERS.MIN.DURATION_TO_STOP, WHEEL_PARAMETERS.MAX.DURATION_TO_STOP);

        this.wheel = new Wheel("wheel.png", speed, duration);
        this.wheel.setPosition(this.game.width * 0.5, this.game.height * 0.5);
        this.addChild(this.wheel.group);

        this.rewardText = new PIXI.Text("Result:   0", { fontFamily: "Arial", fontSize: 50 });
        this.rewardText.anchor.set(0.5);
        this.addChild(this.rewardText);

        this.rewardText.x = this.game.width * 0.8;
        this.rewardText.y = this.game.height * 0.1;


        this.buttonStart = new Button("button_white_disable.png");
        this.buttonStart.text.setText("Start wheel");
        this.buttonStart.setPosition(this.game.width * 0.1 + this.buttonStart.group.width * 0.2,
            this.game.height * 0.1);
        this.addChild(this.buttonStart.group);

        this.buttonStop = new Button("button_white_disable.png");
        this.buttonStop.text.setText("Stop wheel");
        this.buttonStop.setPosition(this.buttonStart.group.x + 10 + this.buttonStart.group.width,
            this.buttonStart.group.y);
        this.addChild(this.buttonStop.group);

        this.setupUIBehaviour();
    }

    setupUIBehaviour() {
        this.buttonStart.addOnDown(() => {
            if (this.wheel.isFastRotating) {
                return;
            }
            this.wheel.delta = 0;
            this.wheel.isFastRotating = true;
            this.buttonStart.setTexture("button_white_press.png");


            let start = (new Date()).getTime();
            this.wheel.speed = HelperFunctions.getRandomArbitrary(0.05, 0.2);

            this.game.addTickerFunction(() => {
                this.wheel.spin(start, () => {
                    this.wheel.isFastRotating = false;
                    this.wheel.isSlowRotating = false;
                    this.game.clearTickerFunctions();
                    let reward = slices[this.wheel.currentSector];
                    this.rewardText.setText("Result:   " + reward);
                });
            });

        });

        this.buttonStop.addOnDown(() => {
            if (!this.wheel.isFastRotating || this.wheel.isSlowRotating) {
                return;
            }
            this.wheel.isSlowRotating = true;
            this.buttonStop.setTexture("button_white_press.png");

            let start = (new Date()).getTime();
            this.game.addTickerFunction(() => {
                this.wheel.stopSpin(start);
            });
        });
    }
}


(function() {
    const assets = ["assets/spritesheet-1.json"];
    const assetsLoader = new PIXI.loaders.Loader();
    assetsLoader.add(assets).load();

    assetsLoader.onComplete.add(() => {
        let game = new Game(window.innerWidth, window.innerHeight);

        document.getElementById("game").appendChild(game.view);

        let scene = new Scene(game);
        game.setState(scene);

        window.addEventListener("resize", () => game.resize());
    });
}());
